# Corewire - Training and Consulting - Demo Application

This is an example project that shows how a Java projects can be compiled and run using Docker multistage builds. It is part of a show case in the [Docker training](https://labs.corewire.de/de/docker/) by [corewire](https://corewire.de/).
